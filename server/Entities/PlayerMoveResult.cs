namespace server.Entities {
    public enum PlayerMoveResult
    {
        validMove,
        InvalidMove,
        WinningMove,
        DrawMove,
    }
}
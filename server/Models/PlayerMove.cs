using server.Entities;

namespace server.Models
{
    public class PlayerMove
    {
        public int row { get; set; }
        public int col { get; set; }
        public string mark { get; set; }
        public PlayerMoveResult result { get; set; }
    }


}
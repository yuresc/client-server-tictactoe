using System;

namespace server
{
    public class WeatherForecast
    {
        private int _xxx;
        public DateTime Date { get; set; }

        public int TemperatureC
        {
            get
            {
                return _xxx;
            }
            set
            {
                _xxx = value;
            }
        }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}

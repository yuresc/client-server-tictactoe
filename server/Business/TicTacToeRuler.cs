using System;
using System.Collections.Generic;
using System.Linq;
using server.Entities;
using server.Models;

namespace server.Business
{
    public class TicTacToeRuler
    {
        private string[] board;
        private string blank = "_";
        private int maxRowLength = 3;

        private void invalidMoveErrorPrompt(int errorType)
        {
            Console.WriteLine("Invalid move error ", errorType);
            if (errorType == 00)
            {
                Console.WriteLine("Must be in the scope of the board size.");
            }

            if (errorType == 01)
            {
                Console.WriteLine("There is already a mark there. Try somewhere else.");
            }
        }
        public bool IsValidMove(int row, int col)
        {
            var space = this.FindSpace(row, col);

            //checks if out of board scope
            if (space < 0 || this.board[space] == null)
            {
                invalidMoveErrorPrompt(0);
                return false;
            }

            //checks if space is marked
            if (this.board[space] != this.blank)
            {
                invalidMoveErrorPrompt(1);
                return false;
            }

            return true;
        }

        private int FindSpace(int row, int col)
        {
            int result;
            if (col > maxRowLength || col < 0)
            {
                result = -1;
                return result;
            }

            if (row > board.Length / maxRowLength || row < 0)
            {
                result = -1;
                return result;
            }
            result = row * this.maxRowLength + col;
            return result;
        }

        private bool IsHorizontalWin(string mark)
        {
            string[] currentRow = new String[maxRowLength];
            for (int i = 0; i < board.Length; i++)
            {
                if (i % maxRowLength == 0)
                {
                    currentRow = board.SubArray(i, i + maxRowLength);
                }
            };
            bool isAMatch = currentRow.All(space => space == mark);
            return isAMatch;

        }

        private bool IsVerticalWin(string mark)
        {
            List<string> currentCol = new List<string>();
            for (int j = 0; j < maxRowLength; j++)
            {
                currentCol.Add(board[j]);
                currentCol.Add(board[j + maxRowLength]);
                currentCol.Add(board[j + (maxRowLength * 2)]);

                if (currentCol.All(space => space == mark))
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsDiagonalWin(string mark)
        {
            return (board[0] == mark &&
                 board[4] == mark &&
                 board[8] == mark)
             ||
             (board[2] == mark &&
                 board[4] == mark &&
                 board[6] == mark);
        }

        private bool IsGameWon(string mark) =>
            IsHorizontalWin(mark) ||
            IsVerticalWin(mark) ||
            IsDiagonalWin(mark);

        private bool IsDraw() =>
            !board.Any(space => space == blank);

        private void PlaceMark(int space, string mark)
        {
            board[space] = mark;
        }

        private string GetMark(int row, int col)
        {
            int space = FindSpace(row, col);
            return board[space];
        }

        private PlayerMove PlayerMove(PlayerMove move)
        {
            if (!IsValidMove(move.row, move.col))
            {
                move.result = PlayerMoveResult.InvalidMove;
                return move;
            }

            if (IsGameWon(move.mark))
            {
                move.result = PlayerMoveResult.WinningMove;
                return move;
            }

            if (IsDraw())
            {
                move.result = PlayerMoveResult.DrawMove;
                return move;
            }

            return move;
        }

    }
}